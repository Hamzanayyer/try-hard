// exercice 1 : créer une fonction qui prend en paramètre un nombre entier et qui
// affiche tous les entiers entre 0 et lui même


// exercice 2 : créer une fonction qui prend en paramètre 2 nombres entiers et qui
// affiche tous les entiers entre le premier et le second


// exercice 3 : créer une fonction qui génére un tirage de loto avec
// 7 nombres compris entre 1 et 45

// exercice 4 : générer un tableau contenant des nombres pairs consécutifs,
// le premier nombre du tableau doit être 4,
// on doit arreter de remplir le tableau quand il y a 20 nombres pairs dans le tableau

// exercice 5 : générer un array avec 100 objets avec la forme ci dessous, dont les données sont toutes aléatoires

// const person = {firstName: '', lastName: '', age: 0};
// console.log(person);

// bonus : calculer la moyenne des âges de personnes en utilisant un reduce


// exercice 6 : Écrire un programme qui affiche les nombres de 1 à 199 avec
// un console log.
// Mais pour les multiples de 3, afficher “Fizz” au lieu du nombre et pour les multiples de 5 afficher “Buzz”.
// Pour les nombres multiples de 3 et 5, afficher “FizzBuzz”.
